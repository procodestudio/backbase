'use strict';

(function() {
  angular
    .module('backbase.main')
    .service('CitiesModel', CitiesModel);

  CitiesModel.$inject = ['CityData'];

  /**
   * Load Cities model
   */
  function CitiesModel(CityData) {
    var model = this;

    model.loadWeather = loadWeather;

    /**
     * Load cities data
     */
    function loadWeather(city) {
      return CityData.getWeather(city).then(function(data) {
        return data;
      });
    }
  }
})();
