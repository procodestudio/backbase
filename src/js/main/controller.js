'use strict';

(function() {
  angular
    .module('backbase.main')
    .controller('MainController', MainController);

  MainController.$inject = ['$state', '$filter'];

  /**
   * Controller initialization
   */
  function MainController($state, $filter) {
    var main = this;

    main.cities = ['Amsterdam', 'London', 'Paris', 'Madrid', 'Rome'];
    main.openForecast = openForecast;

    /**
     * Open weather forecast
     */
    function openForecast(city) {
      var citySlug = $filter('slugify')(city);
      $state.go('forecast', {city: citySlug});
    }
  }
})();
