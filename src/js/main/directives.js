'use strict';

(function() {
  angular
    .module('backbase.main')
    .directive('backbaseCity', backbaseCity);

  backbaseCity.$inject = ['CitiesModel'];

  function backbaseCity(CitiesModel) {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        city: '=',
        openForecast: '&'
      },
      templateUrl: 'templates/city.tpl.html',
      link: loadWeather
    };

    /**
     * Load weather data
     */
    function loadWeather(scope) {
      CitiesModel.loadWeather(scope.city).then(function(data) {
        scope.cityName = data.name;
        scope.temperature = data.temperature;
        scope.windSpeed = data.wind;
        scope.weatherDescription = data.description;
      });
    }
  }
})();
