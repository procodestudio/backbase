describe('MainController', function() {
  var MainController;
  var state;

  beforeEach(function() {
    module('backbase');
    module('templates');

    inject(function($injector, $controller, $state) {
      MainController = $controller('MainController');
      state = $state;
      spyOn(state, 'go');
    });
  });

  it('controller should exists', function() {
    expect(MainController).toBeDefined();
  });

  describe('openForecast()', function() {
    it('should exists', function() {
      expect(MainController.openForecast).toBeDefined();
    });

    it('should open forecast page', function() {
      MainController.openForecast('Amsterdam');
      expect(state.go).toHaveBeenCalled();
    });
  });
});
