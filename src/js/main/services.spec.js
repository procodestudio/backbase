'use strict';

describe('Main services', function() {
  var CitiesModel;
  var CityData;

  describe('CitiesModel', function() {
    beforeEach(function() {
      module('backbase');

      inject(function($injector) {
        CitiesModel = $injector.get('CitiesModel');
        CityData = $injector.get('CityData');
      });

      spyOn(CityData, 'getWeather').and.callFake(function() {
        return {
          then: function(callback) {
            return callback({
              name: 'Amsterdam',
              weather: [{description: 'Sunny'}],
              wind: {speed: 3.5},
              main: {temp: 299}
            });
          }
        };
      });
    });

    it('should exists', function() {
      expect(CitiesModel).toBeDefined();
    });

    describe('loadWeather()', function() {
      it('should exists', function() {
        expect(CitiesModel.loadWeather).toBeDefined();
      });

      it('should load current city weather', function() {
        CitiesModel.loadWeather('Amsterdam');
        expect(CityData.getWeather).toHaveBeenCalled();
      });

      it('should get an object as a response', function() {
        var data = CitiesModel.loadWeather('Amsterdam');
        expect(typeof data).toBe('object');
        expect(data.main.temp).toBe(299);
      });
    });
  });
});
