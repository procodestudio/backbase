describe('Main directives', function() {
  var CitiesModel;
  var compile;
  var scope;
  var directiveElem;

  beforeEach(function() {
    module('backbase');
    module('templates');

    inject(function($injector, $compile, $rootScope) {
      CitiesModel = $injector.get('CitiesModel');
      compile = $compile;
      scope = $rootScope.$new();
    });

    spyOn(CitiesModel, 'loadWeather').and.callFake(function() {
      return {
        then: function(callback) {
          return callback({
            name: 'Amsterdam',
            description: 'Sunny',
            wind: 3.5,
            temperature: 299
          });
        }
      };
    });

    directiveElem = getCompiledElement('<backbase-city></backbase-city>');
  });

  it('should create a city block', function() {
    var block = directiveElem.find('.city__name');
    expect(block.length).toBe(1);
  });

  it('should load weather data into template', function() {
    var name = directiveElem.find('.city__name').text();
    var temperature = directiveElem.find('.city__avg-temperature').text();
    var wind = directiveElem.find('.city__wind-strength').text();
    var description = directiveElem.find('.city__weather-description').text();

    expect(name).toBe('Amsterdam');
    expect(temperature).toBe('299');
    expect(wind).toBe('3.5');
    expect(description).toBe('Sunny');
  });

  /**
   * Compiles the directive
   */
  function getCompiledElement(html) {
    var element = angular.element(html);
    var compiledElement = compile(element)(scope);

    scope.$digest();

    return compiledElement;
  }
});
