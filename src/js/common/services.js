'use strict';

(function() {
  angular
    .module('backbase')
    .service('DataService', DataService);

  DataService.$inject = ['$q', '$http'];

  /**
   * Data service
   */
  function DataService($q, $http) {
    return {
      getData: getData
    };

    /**
     * Make HTTP requests
     */
    function getData(config) {
      var request;
      var data = !config.data ? {} : config.data;
      var header = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };

      request = $http({
        cache: false,
        method: config.method,
        data: $.param(data),
        url: config.url,
        headers: header,
        timeout: 10000
      });

      return request.then(handleSuccess, handleError);
    }

    /**
     * Handle success response from server
     */
    function handleSuccess(response) {
      return response.data;
    }

    /**
     * Handle failed response from server
     */
    function handleError(response) {
      return $q.reject('Error ' + response.status);
    }
  }
})();
