'use strict';

(function() {
  angular
    .module('backbase')
    .factory('Constants', Constants)
    .factory('CityData', CityData)
    .factory('CityForecast', CityForecast);

  Constants.$inject = ['appConstants'];
  CityData.$inject = ['Constants', 'DataService'];
  CityForecast.$inject = ['Constants', 'DataService'];

  /**
   * Return a specific constant value
   */
  function Constants(appConstants) {
    return {
      get: getConstant
    };

    /**
     * Return constant value
     */
    function getConstant(key) {
      return appConstants[key];
    }
  }

  /**
   * Get city data
   */
  function CityData(Constants, DataService) {
    return {
      getWeather: getWeather
    };

    /**
     * Return weather info from API
     */
    function getWeather(location) {
      var apiUrl = Constants.get('WEATHER_URL');
      var apiKey = Constants.get('API_KEY');
      var weatherUrl = apiUrl + location + '&appid=' + apiKey;

      var data = DataService
        .getData({
          method: 'GET',
          url: weatherUrl
        });

      return data.then(function(data) {
        return {
          name: data.name,
          wind: data.wind.speed,
          description: data.weather[0].description,
          temperature: data.main.temp
        };
      });
    }
  }

  /**
   * Get city forecast
   */
  function CityForecast(Constants, DataService) {
    return {
      getForecast: getForecast
    };

    /**
     * Return weather info from API
     */
    function getForecast(location) {
      var apiUrl = Constants.get('FORECAST_URL');
      var apiKey = Constants.get('API_KEY');
      var forecastUrl = apiUrl + location + '&appid=' + apiKey;

      var data = DataService
        .getData({
          method: 'GET',
          url: forecastUrl
        });

      return data.then(function(data) {
        /* jshint camelcase: false */
        // jscs:disable requireCamelCaseOrUpperCaseIdentifiers

        var forecasts = data.list;
        var forecast = [];

        for (var i = 0; i < 5; i++) {
          var forecastData = {};

          if (forecasts[i]) {
            forecastData.wind = forecasts[i].wind.speed;
            forecastData.temperature = forecasts[i].main.temp;
            forecastData.description = forecasts[i].weather[0].description;
            forecastData.date = forecasts[i].dt_txt;
            forecast.push(forecastData);
          }
        }

        return forecast;
      });
    }
  }
})();
