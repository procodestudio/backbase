'use strict';

(function() {
  angular
    .module('backbase')
    .filter('slugify', slugify)
    .filter('formatTime', formatTime)
    .filter('formatDate', formatDate);

  /**
   * Convert city name into slug string
   */
  function slugify() {
    return function(input) {
      return input
        .toLowerCase()
        .replace(/[^\w\s-]/g, '')
        .replace(/[\s_-]+/g, '-')
        .replace(/^-+|-+$/g, '');
    };
  }

  /**
   * Extract time from API property
   */
  function formatTime() {
    return function(input) {
      return moment(input).format('HH:mm');
    };
  }

  /**
   * Extract date from API property
   */
  function formatDate() {
    return function(input) {
      return moment(input).format('DD/MM');
    };
  }
})();
