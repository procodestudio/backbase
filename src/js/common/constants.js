'use strict';

(function() {
  var baseURl = 'http://api.openweathermap.org/data/2.5/';

  angular
    .module('backbase')
    .constant('appConstants', {
      TEST: 'Ok',
      WEATHER_URL: baseURl + 'weather?units=metric&q=',
      FORECAST_URL: baseURl + 'forecast?units=metric&q=',
      API_KEY: '6908baeaca21cd9e4381f037638a7b3e'
    });
})();
