'use strict';

describe('Common services', function() {
  var DataService;
  var $httpBackend;
  var urlSuccess = 'http://localhost/backbase';
  var urlError = 'http://localhost/backbase/error';
  var jsonRes = {'id': 2643743, 'name': 'London', 'cod': 200};

  describe('DataService', function() {
    beforeEach(function() {
      module('templates');
      module('backbase');

      inject(function($injector) {
        DataService = $injector.get('DataService');
        $httpBackend = $injector.get('$httpBackend');
        $httpBackend.when('GET', urlSuccess).respond(200, jsonRes);
        $httpBackend.when('GET', urlError).respond(404);
      });
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should exists', function() {
      expect(DataService).toBeDefined();
    });

    describe('getData()', function() {
      it('should exists', function() {
        expect(DataService.getData).toBeDefined();
      });

      it('should return an object when passing data', function() {
        var config = {method: 'GET', url: urlSuccess, data: {city: 'London'}};

        DataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an object when passing no data', function() {
        var config = {method: 'GET', url: urlSuccess};

        DataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an error from promise', function() {
        var config = {method: 'GET', url: urlError};

        DataService
          .getData(config)
          .catch(function(error) {
            expect(error).toBe('Error 404');
          });

        $httpBackend.flush();
      });
    });
  });
});
