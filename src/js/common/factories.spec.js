describe('Common factories', function() {
  var Constants;
  var CityData;
  var CityForecast;
  var DataService;

  beforeEach(function() {
    module('backbase');
  });

  describe('Constants', function() {
    beforeEach(function() {
      inject(function($injector) {
        Constants = $injector.get('Constants');
      });
    });

    it('should exists', function() {
      expect(Constants).toBeDefined();
    });

    describe('get()', function() {
      it('should exists', function() {
        expect(Constants.get).toBeDefined();
      });

      it('should return a constant', function() {
        expect(Constants.get('TEST')).toEqual('Ok');
      });
    });
  });

  describe('CityData', function() {
    beforeEach(function() {
      inject(function($injector) {
        CityData = $injector.get('CityData');
        DataService = $injector.get('DataService');
      });

      spyOn(DataService, 'getData').and.callFake(function() {
        return {
          then: function(callback) {
            return callback({
              name: 'Amsterdam',
              weather: [{description: 'Sunny'}],
              wind: {speed: 3.5},
              main: {temp: 299}
            });
          }
        };
      });
    });

    it('should exists', function() {
      expect(CityData).toBeDefined();
    });

    describe('getWeather()', function() {
      it('should exists', function() {
        expect(CityData.getWeather).toBeDefined();
      });

      it('should return city information from API', function() {
        CityData.getWeather('Amsterdam');
        expect(DataService.getData).toHaveBeenCalled();
      });
    });
  });

  describe('CityForecast', function() {
    beforeEach(function() {
      inject(function($injector) {
        CityForecast = $injector.get('CityForecast');
        DataService = $injector.get('DataService');
      });

      spyOn(DataService, 'getData').and.callFake(function() {
        return {
          then: function(callback) {
            return callback({
              list: [
                {
                  date: 123456,
                  weather: [{description: 'Sunny'}],
                  wind: {speed: 3.5},
                  main: {temp: 299}
                }
              ]
            });
          }
        };
      });
    });

    it('should exists', function() {
      expect(CityForecast).toBeDefined();
    });

    describe('getForecast()', function() {
      it('should exists', function() {
        expect(CityForecast.getForecast).toBeDefined();
      });

      it('should return forecast information from API', function() {
        CityForecast.getForecast('Amsterdam');
        expect(DataService.getData).toHaveBeenCalled();
      });
    });
  });
});
