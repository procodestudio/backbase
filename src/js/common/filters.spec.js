'use strict';

describe('Common filters', function() {
  var $filter;

  beforeEach(function() {
    module('backbase');

    inject(function($injector) {
      $filter = $injector.get('$filter');
    });
  });

  describe('slugify()', function() {
    it('should return slug from city name', function() {
      var output;
      var input = 'Porto Madero';

      output = $filter('slugify')(input);

      expect(output).toEqual('porto-madero');
    });
  });

  describe('formatDate()', function() {
    it('should extract date from string', function() {
      var output;
      var input = '2017-08-09 09:00:00';

      output = $filter('formatDate')(input);

      expect(output).toEqual('09/08');
    });
  });

  describe('formatTime()', function() {
    it('should extract time from string', function() {
      var output;
      var input = '2017-08-09 09:00:00';

      output = $filter('formatTime')(input);

      expect(output).toEqual('09:00');
    });
  });
});
