'use strict';

(function() {
  angular
    .module('backbase.forecast')
    .service('ForecastModel', ForecastModel);

  ForecastModel.$inject = ['CityForecast'];

  /**
   * Load Cities model
   */
  function ForecastModel(CityForecast) {
    var model = this;

    model.load = loadForecast;

    /**
     * Load cities data
     */
    function loadForecast(city) {
      return CityForecast.getForecast(city).then(function(data) {
        return data;
      });
    }
  }
})();
