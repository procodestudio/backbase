'use strict';

describe('Forecast module', function() {
  var $stateProvider;

  beforeEach(function() {
    module('ui.router');
    module(function(_$stateProvider_) {
      $stateProvider = _$stateProvider_;
      spyOn($stateProvider, 'state');
    });
    module('backbase.forecast');
    inject();
  });

  it('should setup module router', function() {
    expect($stateProvider.state).toHaveBeenCalled();
  });
});
