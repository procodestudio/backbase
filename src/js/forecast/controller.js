'use strict';

(function() {
  angular
    .module('backbase.forecast')
    .controller('ForecastController', ForecastController);

  ForecastController.$inject = ['$stateParams', '$filter', 'ForecastModel'];

  /**
   * Controller initialization
   */
  function ForecastController($stateParams, $filter, ForecastModel) {
    var forecast = this;

    forecast.$onInit = getForecast;
    forecast.getForecast = getForecast;
    forecast.days = null;
    forecast.chartConfig = {
      series: ['Wind (m/s)', 'Temperature (°C)'],
      data: [],
      datasetOverride: [
        {yAxisID: 'y-axis-1'},
        {yAxisID: 'y-axis-2'}
      ],
      options: {
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left'
            },
            {
              id: 'y-axis-2',
              type: 'linear',
              display: true,
              position: 'right'
            }
          ]
        }
      }
    };

    /**
     * Get forecast data
     */
    function getForecast() {
      forecast.cityName = $stateParams.city;

      return ForecastModel.load($stateParams.city).then(function(data) {
        forecast.days = data;
        getChartData(data);
      });
    }

    /**
     * Extract values to feed the chart
     */
    function getChartData(data) {
      forecast.chartConfig.data.push(_.map(data, 'wind'));
      forecast.chartConfig.data.push(_.map(data, 'temperature'));
      forecast.chartConfig.labels = _.map(data, function(value) {
        return $filter('formatTime')(value.date) + 'h';
      });
    }
  }
})();
