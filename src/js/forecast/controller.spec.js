describe('ForecastController', function() {
  var ForecastController;
  var ForecastModel;
  var jsonResponse = {
    name: 'Amsterdam',
    description: 'Sunny',
    wind: 3.5,
    temperature: 299
  };

  beforeEach(function() {
    module('backbase');
    module('templates');

    inject(function($injector, $controller) {
      ForecastController = $controller('ForecastController');
      ForecastModel = $injector.get('ForecastModel');
    });

    spyOn(ForecastModel, 'load').and.callFake(function() {
      return {
        then: function(callback) {
          return callback(jsonResponse);
        }
      };
    });
  });

  it('controller should exists', function() {
    expect(ForecastController).toBeDefined();
  });

  describe('getForecast()', function() {
    it('should exists', function() {
      expect(ForecastController.getForecast).toBeDefined();
    });

    it('should get city forecast', function() {
      ForecastController.$onInit();
      expect(ForecastModel.load).toHaveBeenCalled();
    });

    it('should assign forecast data to days variable', function() {
      expect(ForecastController.days).toBeNull();
      ForecastController.getForecast();
      expect(ForecastController.days).toBe(jsonResponse);
    });
  });
});
