'use strict';

(function() {
  angular
    .module('backbase.forecast', [])
    .config(forecastStateProvider);

  forecastStateProvider.$inject = ['$stateProvider'];

  /**
   * Configure module state provider
   */
  function forecastStateProvider($stateProvider) {
    $stateProvider
      .state('forecast', {
        url: '/:city',
        templateUrl: 'templates/forecast.tpl.html',
        controller: 'ForecastController as forecast'
      });
  }
})();
