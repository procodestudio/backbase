'use strict';

describe('Forecast services', function() {
  var ForecastModel;
  var CityForecast;

  describe('ForecastModel', function() {
    beforeEach(function() {
      module('backbase');

      inject(function($injector) {
        ForecastModel = $injector.get('ForecastModel');
        CityForecast = $injector.get('CityForecast');
      });

      spyOn(CityForecast, 'getForecast').and.callFake(function() {
        return {
          then: function(callback) {
            return callback({
              list: [
                {
                  date: 123456,
                  weather: [{description: 'Sunny'}],
                  wind: {speed: 3.5},
                  main: {temp: 299}
                }
              ]
            });
          }
        };
      });
    });

    it('should exists', function() {
      expect(ForecastModel).toBeDefined();
    });

    describe('load()', function() {
      it('should exists', function() {
        expect(ForecastModel.load).toBeDefined();
      });

      it('should load current city weather', function() {
        ForecastModel.load('Amsterdam');
        expect(CityForecast.getForecast).toHaveBeenCalled();
      });

      it('should get an object as a response', function() {
        var data = ForecastModel.load('Amsterdam');
        expect(typeof data).toBe('object');
        expect(data.list[0].date).toBe(123456);
      });
    });
  });
});
