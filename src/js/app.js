'use strict';

(function() {
  var modules = [
    'chart.js',
    'ui.router',
    'backbase.main',
    'backbase.forecast'
  ];

  angular
    .module('backbase', modules)
    .config(appConfig);

  appConfig.$inject = ['$locationProvider', '$urlRouterProvider', '$qProvider'];

  /**
   * Initial config
   */
  function appConfig($locationProvider, $urlRouterProvider, $qProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $qProvider.errorOnUnhandledRejections(false);
  }
})();
